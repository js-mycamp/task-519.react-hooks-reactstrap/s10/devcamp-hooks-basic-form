import { useEffect, useState } from "react"

function UserForm() {
    const [fistName, setFistName] = useState(localStorage.getItem("firstName"));
    const [lastName, setLastName] = useState(localStorage.getItem("lastName"));
    const handleFistNameChange = (event) => {
        console.log(event.target.value);
        setFistName(event.target.value)
    }
    const handleLastNameChange = (event) => {
        console.log(event.target.value);
        setLastName(event.target.value)
    }
    useEffect(() => {
        localStorage.setItem("firstName",fistName)
        localStorage.setItem("lastName",lastName)
    })
    return (
        <div style={{"backgroundColor" : "#1B2533",
        "text-align": "center",
        "width": "300px", 
        "position": "absolute",
        "top": "30%",
        "left": "45%",
        "padding": "15px"}}>

            <input  value={fistName} onChange={handleFistNameChange} placeholder="devcamp" style={{"backgroundColor" :"#8BDFEB","width" : "200px", "height":"45px",  }}/>
            <br></br>
            <input value={lastName} onChange={handleLastNameChange} placeholder="user" style={{"backgroundColor" :"#8BDFEB","width" : "200px", "height":"45px"}} />
            <h1 style={{"color" :"#8BDFEB",}}>{fistName} {lastName}</h1>

        </div>
    )
}
export default UserForm